package conf

import (
	"flag"
	"github.com/spf13/viper"
	"log"
	"os"
)

type AppConfig struct {
	Server   ServerSettings
	Database DatabaseSettings
	Security SecuritySettings
}

type ServerSettings struct {
	Protocol string
	Port     string
}

type DatabaseSettings struct {
	Port     string
	Host     string
	DbName   string
	Username string
	Password string
}

type SecuritySettings struct {
	ApiKey string
}

const configDir = "/server/conf/"

func NewConfig(appProfile string) (config AppConfig) {

	if _, err := os.Stat(configDir + appProfile + ".yml"); err != nil {
		log.Fatalf("Wrong config file name, %s", err)
	}

	viper.SetConfigName(appProfile)
	viper.AddConfigPath(configDir)

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Error reading config file, %s", err)
	}

	if err := viper.Unmarshal(&config); err != nil {
		log.Fatalf("Unable to decode into struct, %v", err)
	}

	return config
}

func LoadProfileFlag() *string {
	profile := flag.String("p", "local", "")
	flag.Parse()
	return profile
}
