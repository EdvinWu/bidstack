package main

import (
	"bidstack.hw/server/conf"
	"bidstack.hw/server/db"
	"bidstack.hw/server/player"
	"bidstack.hw/server/server"
)

func main() {
	config := conf.NewConfig(*conf.LoadProfileFlag())
	db.MigrateDB(config.Database)
	repository := player.NewPlayerRepository(db.OpenConnection(config.Database))
	service := player.NewPlayerService(repository)
	serv := server.CreateServer(config.Security, config.Server, &service)
	serv.ServerRun()
}
