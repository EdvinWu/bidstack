package db

import (
	"bidstack.hw/server/conf"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
)

type DataConnection struct {
	gorm.DB
}

func OpenConnection(settings conf.DatabaseSettings) *DataConnection {
	db, err := gorm.Open("postgres", createGormUrl(settings))
	if err != nil {
		log.Fatal(err)
	}

	return &DataConnection{*db}
}

func createGormUrl(settings conf.DatabaseSettings) string {
	return fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		settings.Host,
		settings.Port,
		settings.Username,
		settings.Password,
		settings.DbName)
}
