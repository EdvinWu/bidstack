package db

import (
	"bidstack.hw/server/conf"
	"fmt"
	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
)

func MigrateDB(settings conf.DatabaseSettings) {
	m, err := migrate.New("file://migrate", createMigrateUrl(settings))
	if err != nil {
		panic(err)
	}

	err = m.Up()
	if err != migrate.ErrNoChange && err != nil {
		panic(err)
	}
}

func createMigrateUrl(settings conf.DatabaseSettings) string {
	return fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", settings.Username,
		settings.Password,
		settings.Host,
		settings.Port,
		settings.DbName)
}
