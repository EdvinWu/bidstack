package server

import (
	leaderboardpb "bidstack.hw/protobuf"
	"bidstack.hw/server/conf"
	"bidstack.hw/server/entity"
	"bidstack.hw/server/player"
	"context"
	"encoding/base64"
	"fmt"
	grpcauth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	"github.com/grpc-ecosystem/go-grpc-middleware/util/metautils"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"io"
	"log"
	"net"
	"strings"
)

type Server struct {
	security      conf.SecuritySettings
	servConfig    conf.ServerSettings
	playerService *player.Service
}

func CreateServer(config conf.SecuritySettings, servConf conf.ServerSettings, service *player.Service) *Server {
	return &Server{security: config, servConfig: servConf, playerService: service}
}

func (s *Server) mapToProto(pagedPlayers []entity.Player) []*leaderboardpb.Player {
	protoPlayers := make([]*leaderboardpb.Player, 0)
	if pagedPlayers != nil {
		for _, pl := range pagedPlayers {
			protoPlayers = append(protoPlayers, pl.MapToProtoPlayer())
		}
	}
	return protoPlayers
}

func (s *Server) authenticate(ctx context.Context) (context.Context, error) {
	apiKey := metautils.ExtractIncoming(ctx).Get("Api-Key")

	decodedApiKey, _ := base64.StdEncoding.DecodeString(apiKey)
	i := string(decodedApiKey)
	if strings.Compare(i, s.security.ApiKey) != 0 {
		return nil, status.Error(codes.Unauthenticated, "invalid auth token")
	}
	newCtx := context.WithValue(ctx, "token", apiKey)
	return newCtx, nil
}

func (s *Server) ServerRun() {
	serverUrl := fmt.Sprintf(":%s", s.servConfig.Port)
	if lis, err := net.Listen(s.servConfig.Protocol, serverUrl); err != nil {
		log.Fatalf("Failed to listen: %v", err)
	} else {

		grpcServer := grpc.NewServer(
			grpc.UnaryInterceptor(
				grpcauth.UnaryServerInterceptor(s.authenticate),
			),
			grpc.StreamInterceptor(
				grpcauth.StreamServerInterceptor(s.authenticate),
			),
		)

		service := leaderboardService{playerService: *s.playerService}
		leaderboardpb.RegisterLeaderboardServiceServer(grpcServer, &service)

		log.Printf("App is up and running on port: %v", s.servConfig.Port)

		if err := grpcServer.Serve(lis); err != nil {
			log.Fatalf("Failed to run server: %v", err)
		}
	}
}

type leaderboardService struct {
	playerService player.Service
}

func (ls *leaderboardService) StorePlayers(stream leaderboardpb.LeaderboardService_StorePlayersServer) error {
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			res := &leaderboardpb.StoreScoreResponse{}
			return stream.SendAndClose(res)
		}
		if err != nil {
			return err
		}

		err = ls.playerService.SavePlayer(req)
		if err != nil {
			return err
		}
	}
}

func (ls *leaderboardService) GetPlayers(ctx context.Context, req *leaderboardpb.GetPlayerRequest) (*leaderboardpb.GetPlayerResponse, error) {
	pagedPlayers, err := ls.playerService.GetPlayersPage(req.Name, req.Page, req.PageSize)
	res := &leaderboardpb.GetPlayerResponse{
		PagedPlayers:  ls.mapToProto(pagedPlayers.Players),
		PlayersAround: ls.mapToProto(pagedPlayers.PlayersAround),
		NextPage:      pagedPlayers.NextPage,
	}
	if err != nil {
		log.Printf("Error occurred during response: %v", err)
	}
	return res, err
}

func (ls *leaderboardService) mapToProto(pagedPlayers []entity.Player) []*leaderboardpb.Player {
	protoPlayers := make([]*leaderboardpb.Player, 0)
	if pagedPlayers != nil {
		for _, pl := range pagedPlayers {
			protoPlayers = append(protoPlayers, pl.MapToProtoPlayer())
		}
	}
	return protoPlayers
}
