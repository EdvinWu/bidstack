package player

import (
	leaderboardpb "bidstack.hw/protobuf"
	"bidstack.hw/server/entity"
	"fmt"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"testing"
)

func TestFindPageWithoutName(t *testing.T) {

	playerRepositoryMock := new(PlayerRepoMock)
	playerService := NewPlayerService(playerRepositoryMock)

	playerRepositoryMock.
		On("FindPage", int64(1), int64(5)).
		Return(createPlayers(), nil)

	page, e := playerService.GetPlayersPage("", 1, 5)

	assrt := assert.New(t)
	assrt.NoError(e)
	assrt.Equal([]entity.Player(nil), page.PlayersAround)
	assrt.Equal(int64(2), page.NextPage)
	assrt.Equal("id0", page.Players[0].Id)
	assrt.Equal("name0", page.Players[0].Name)
	assrt.Equal(int64(0), page.Players[0].Score)
	assrt.Equal(int64(5), page.Players[0].Rank)
	assrt.Equal(5, len(page.Players))
}

func TestFindPageDBError(t *testing.T) {

	playerRepositoryMock := new(PlayerRepoMock)
	playerService := NewPlayerService(playerRepositoryMock)

	playerRepositoryMock.
		On("FindPage", int64(1), int64(5)).
		Return([]entity.Player(nil), gorm.ErrUnaddressable)

	page, e := playerService.GetPlayersPage("", 1, 5)

	assrt := assert.New(t)
	assrt.Error(e)
	assrt.Equal(PlayersPage{}, page)

}

func TestFindWithNameAlreadyFound(t *testing.T) {

	playerRepositoryMock := new(PlayerRepoMock)
	playerService := NewPlayerService(playerRepositoryMock)

	playerRepositoryMock.
		On("FindPage", int64(1), int64(5)).
		Return(createPlayers(), nil)

	page, e := playerService.GetPlayersPage("name0", 1, 5)

	assrt := assert.New(t)
	assrt.NoError(e)
	assrt.Equal([]entity.Player(nil), page.PlayersAround)
	assrt.Equal(int64(2), page.NextPage)
	assrt.Equal("id0", page.Players[0].Id)
	assrt.Equal("name0", page.Players[0].Name)
	assrt.Equal(int64(0), page.Players[0].Score)
	assrt.Equal(int64(5), page.Players[0].Rank)
	assrt.Equal(5, len(page.Players))
}

func TestFindWithName(t *testing.T) {

	playerRepositoryMock := new(PlayerRepoMock)
	playerService := NewPlayerService(playerRepositoryMock)

	playerRepositoryMock.
		On("FindPage", int64(1), int64(5)).
		Return(createPlayers(), nil)

	playerRepositoryMock.On("FindByName", "name10").
		Return(entity.Player{Rank: 6, Score: 0, Name: "name10", Id: "id10"}, nil)

	playerRepositoryMock.On("FindByScoreMore", int64(0), int64(3)).
		Return(createPlayersMore(), nil)

	playerRepositoryMock.On("FindByScoreLess", int64(0), int64(3)).
		Return([]entity.Player(nil), nil)

	page, e := playerService.GetPlayersPage("name10", 1, 5)

	assrt := assert.New(t)
	assrt.NoError(e)
	assrt.Equal("name2", page.PlayersAround[0].Name)
	assrt.Equal(int64(3), page.PlayersAround[0].Rank)
	assrt.Equal(int64(3), page.PlayersAround[0].Score)
	assrt.Equal("id2", page.PlayersAround[0].Id)
	assrt.Equal(4, len(page.PlayersAround))
	assrt.Equal("name10", page.PlayersAround[3].Name)
	assrt.Equal(int64(2), page.NextPage)
	assrt.Equal("id0", page.Players[0].Id)
	assrt.Equal("name0", page.Players[0].Name)
	assrt.Equal(int64(0), page.Players[0].Score)
	assrt.Equal(int64(5), page.Players[0].Rank)
	assrt.Equal(5, len(page.Players))
}

func TestAlreadyFound(t *testing.T) {

	playerRepositoryMock := new(PlayerRepoMock)
	playerService := service{playerRepository: playerRepositoryMock}

	found := playerService.alreadyFound(createPlayers(), "name3")

	assrt := assert.New(t)
	assrt.True(found)
}

func TestAlreadyFoundFalse(t *testing.T) {

	playerRepositoryMock := new(PlayerRepoMock)
	playerService := service{playerRepository: playerRepositoryMock}

	found := playerService.alreadyFound(createPlayers(), "name3333")

	assrt := assert.New(t)
	assrt.False(found)
}

func TestCollectPlayersAroundNoPlayerByName(t *testing.T) {

	playerRepositoryMock := new(PlayerRepoMock)
	playerService := service{playerRepository: playerRepositoryMock}

	playerRepositoryMock.On("FindByName", "name10").
		Return(entity.Player{}, gorm.ErrRecordNotFound)

	players, e := playerService.collectPlayersAround("name10")

	assrt := assert.New(t)
	assrt.Error(e)
	assrt.Equal([]entity.Player(nil), players)

}

func TestCollectPlayersAroundErrorFindLess(t *testing.T) {

	playerRepositoryMock := new(PlayerRepoMock)
	playerService := service{playerRepository: playerRepositoryMock}

	playerRepositoryMock.On("FindByName", "name10").
		Return(entity.Player{Rank: 6, Score: 0, Name: "name10", Id: "id10"}, nil)

	playerRepositoryMock.On("FindByScoreMore", int64(0), int64(3)).
		Return(createPlayersMore(), nil)

	playerRepositoryMock.On("FindByScoreLess", int64(0), int64(3)).
		Return([]entity.Player(nil), gorm.ErrRecordNotFound)

	players, e := playerService.collectPlayersAround("name10")

	assrt := assert.New(t)
	assrt.Error(e)
	assrt.Equal([]entity.Player(nil), players)

}

func TestCollectPlayersAroundErrorFindMore(t *testing.T) {

	playerRepositoryMock := new(PlayerRepoMock)
	playerService := service{playerRepository: playerRepositoryMock}

	playerRepositoryMock.On("FindByName", "name10").
		Return(entity.Player{Rank: 6, Score: 0, Name: "name10", Id: "id10"}, nil)

	playerRepositoryMock.On("FindByScoreMore", int64(0), int64(3)).
		Return([]entity.Player(nil), gorm.ErrRecordNotFound)

	players, e := playerService.collectPlayersAround("name10")

	assrt := assert.New(t)
	assrt.Error(e)
	assrt.Equal([]entity.Player(nil), players)

}

func TestCollectAround(t *testing.T) {

	playerRepositoryMock := new(PlayerRepoMock)
	playerService := service{playerRepository: playerRepositoryMock}

	playerRepositoryMock.On("FindByName", "name10").
		Return(entity.Player{Rank: 6, Score: 0, Name: "name10", Id: "id10"}, nil)

	playerRepositoryMock.On("FindByScoreMore", int64(0), int64(3)).
		Return(createPlayersMore(), nil)

	playerRepositoryMock.On("FindByScoreLess", int64(0), int64(3)).
		Return(createPlayersLess(), nil)

	around, e := playerService.collectPlayersAround("name10")

	assrt := assert.New(t)
	assrt.NoError(e)
	assrt.Equal("name2", around[0].Name)
	assrt.Equal(int64(3), around[0].Rank)
	assrt.Equal(int64(3), around[0].Score)
	assrt.Equal("id2", around[0].Id)
	assrt.Equal(7, len(around))
	assrt.Equal("name10", around[3].Name)
	assrt.Equal("name8", around[6].Name)

}

func TestSave(t *testing.T) {
	playerRepositoryMock := new(PlayerRepoMock)
	playerService := service{playerRepository: playerRepositoryMock}

	playerRepositoryMock.On("FindByName", "name").
		Return(entity.Player{}, gorm.ErrRecordNotFound)

	match := mock.MatchedBy(func(player Player) bool {
		if player.Score == 12 && player.Name == "name" {
			return true
		}
		return false
	})

	player := leaderboardpb.Player{Score: 12, Name: "name"}
	playerRepositoryMock.On("Save", match).
		Return(nil)

	e := playerService.SavePlayer(&leaderboardpb.StoreScoreRequest{Player: &player})

	assrt := assert.New(t)
	assrt.NoError(e)
}

func TestSaveScoreHigher(t *testing.T) {
	playerRepositoryMock := new(PlayerRepoMock)
	playerService := service{playerRepository: playerRepositoryMock}

	playerRepositoryMock.On("FindByName", "name").
		Return(entity.Player{Score: 14, Name: "name", Id: "idi"}, nil)

	match := mock.MatchedBy(func(player Player) bool {
		if player.Score == 14 && player.Name == "name" && player.Id == "idi" {
			return true
		}
		return false
	})

	player := leaderboardpb.Player{Score: 12, Name: "name"}
	playerRepositoryMock.On("Save", match).
		Return(nil)

	e := playerService.SavePlayer(&leaderboardpb.StoreScoreRequest{Player: &player})

	assrt := assert.New(t)
	assrt.NoError(e)
}

func TestSaveFailed(t *testing.T) {
	playerRepositoryMock := new(PlayerRepoMock)
	playerService := service{playerRepository: playerRepositoryMock}

	playerRepositoryMock.On("FindByName", "name").
		Return(entity.Player{}, gorm.ErrInvalidSQL)

	player := leaderboardpb.Player{Score: 12, Name: "name"}
	playerRepositoryMock.On("Save").
		Return(nil)

	e := playerService.SavePlayer(&leaderboardpb.StoreScoreRequest{Player: &player})

	assrt := assert.New(t)
	assrt.Error(e)
}

func TestSaveScoreLower(t *testing.T) {
	playerRepositoryMock := new(PlayerRepoMock)
	playerService := service{playerRepository: playerRepositoryMock}

	playerRepositoryMock.On("FindByName", "name").
		Return(entity.Player{Id: "idi", Name: "name", Score: 10}, nil)

	match := mock.MatchedBy(func(player Player) bool {
		if player.Score == 12 && player.Name == "name" && player.Id == "idi" {
			return true
		}
		return false
	})

	player := leaderboardpb.Player{Score: 12, Name: "name"}
	playerRepositoryMock.On("Save", match).
		Return(nil)

	e := playerService.SavePlayer(&leaderboardpb.StoreScoreRequest{Player: &player})

	assrt := assert.New(t)
	assrt.NoError(e)
}

func createPlayersLess() interface{} {
	player := entity.Player{Id: "id6",
		Name: "name6", Score: -1, Rank: 7}
	player2 := entity.Player{Id: "id7",
		Name: "name7", Score: -2, Rank: 8}
	player3 := entity.Player{Id: "id8",
		Name: "name8", Score: -3, Rank: 9}

	return []entity.Player{player2, player, player3}
}

func createPlayersMore() []entity.Player {
	player := entity.Player{Id: "id4",
		Name: "name4", Score: 1, Rank: 5}
	player2 := entity.Player{Id: "id3",
		Name: "name3", Score: 2, Rank: 4}
	player3 := entity.Player{Id: "id2",
		Name: "name2", Score: 3, Rank: 3}

	return []entity.Player{player2, player, player3}

}

func createPlayers() []entity.Player {
	b := make([]entity.Player, 0, 5)
	for i := 0; i < 5; i++ {
		b = append(b, entity.Player{Id: fmt.Sprintf("id%d", i),
			Name:  fmt.Sprintf("name%d", i),
			Score: int64(i),
			Rank:  int64(5 - i)})
	}
	return b
}

type PlayerRepoMock struct {
	mock.Mock
}

func (p PlayerRepoMock) Save(player Player) error {
	args := p.Called(player)
	return args.Error(0)
}

func (p PlayerRepoMock) FindByName(name string) (entity.Player, error) {
	args := p.Called(name)
	return args.Get(0).(entity.Player), args.Error(1)
}

func (p PlayerRepoMock) FindPage(pageNumber, pageSize int64) ([]entity.Player, error) {
	args := p.Called(pageNumber, pageSize)
	return args.Get(0).([]entity.Player), args.Error(1)
}

func (p PlayerRepoMock) FindByScoreMore(score, limit int64) ([]entity.Player, error) {
	args := p.Called(score, limit)
	return args.Get(0).([]entity.Player), args.Error(1)
}

func (p PlayerRepoMock) FindByScoreLess(score, limit int64) ([]entity.Player, error) {
	args := p.Called(score, limit)
	return args.Get(0).([]entity.Player), args.Error(1)
}
