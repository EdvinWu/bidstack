package player

import (
	"bidstack.hw/server/db"
	"bidstack.hw/server/entity"
)

type Player struct {
	Id    string
	Name  string
	Score int64
}

const (
	selectWithRank = "select *, rank() over (order by score desc) as rank " +
		"from players "
)

type Repo interface {
	Save(player Player) error
	FindByName(name string) (entity.Player, error)
	FindPage(pageNumber, pageSize int64) ([]entity.Player, error)
	FindByScoreMore(score, limit int64) ([]entity.Player, error)
	FindByScoreLess(score, limit int64) ([]entity.Player, error)
}

type repository struct {
	*db.DataConnection
}

func NewPlayerRepository(dataSource *db.DataConnection) Repo {
	return &repository{DataConnection: dataSource}
}

func (pr *repository) Save(player Player) error {
	return pr.DB.Save(&player).Error
}

func (pr *repository) FindByName(name string) (entity.Player, error) {
	var player entity.Player
	found := pr.DB.Where("name = ?", name).Find(&player)
	if found.Error != nil {
		return player, found.Error
	}
	return player, nil
}

func (pr *repository) FindPage(pageNumber, pageSize int64) ([]entity.Player, error) {
	validPageSize := pr.resolvePageSize(pageSize)
	var players []entity.Player
	found := pr.DB.Raw(selectWithRank+
		"order by score desc offset ? limit ?", pr.resolveOffset(validPageSize, pageNumber), validPageSize).Scan(&players)
	if found.Error != nil {
		return nil, found.Error
	}
	return players, nil
}

func (pr *repository) FindByScoreMore(score, limit int64) ([]entity.Player, error) {
	var players []entity.Player
	found := pr.DB.Raw(selectWithRank+
		"where score > ? order by score limit ?", score, limit).Scan(&players)
	if found.Error != nil {
		return nil, found.Error
	}
	return players, nil
}

func (pr *repository) FindByScoreLess(score, limit int64) ([]entity.Player, error) {
	var players []entity.Player
	found := pr.DB.Raw(selectWithRank+
		"where score < ? order by score limit ?", score, limit).Scan(&players)
	if found.Error != nil {
		return nil, found.Error
	}
	return players, nil
}

func (pr *repository) resolveOffset(pageSize, pageNumber int64) int64 {
	if pageNumber < 2 {
		return 0
	}
	return pageSize * (pageNumber - 1)
}

func (pr *repository) resolvePageSize(pageSize int64) int64 {
	if pageSize < 10 {
		return 10
	}
	return pageSize
}
