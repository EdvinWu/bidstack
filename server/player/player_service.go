package player

import (
	leaderboardpb "bidstack.hw/protobuf"
	"bidstack.hw/server/entity"
	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"sort"
)

type PlayersPage struct {
	Players       []entity.Player
	PlayersAround []entity.Player
	NextPage      int64
}

type Service interface {
	GetPlayersPage(playerName string, pageNumber, pageSize int64) (PlayersPage, error)
	SavePlayer(storeScoreRequest *leaderboardpb.StoreScoreRequest) error
}

type service struct {
	playerRepository Repo
}

func NewPlayerService(playerRepository Repo) Service {
	return &service{playerRepository: playerRepository}
}

func (s service) GetPlayersPage(playerName string, pageNumber, pageSize int64) (PlayersPage, error) {
	players, err := s.playerRepository.FindPage(pageNumber, pageSize)
	if err != nil {
		return PlayersPage{}, status.Error(codes.Internal, err.Error())
	}
	var playersAround []entity.Player
	if playerName != "" && !s.alreadyFound(players, playerName) {
		playersAround, _ = s.collectPlayersAround(playerName)
	}
	return PlayersPage{Players: players, PlayersAround: playersAround, NextPage: pageNumber + 1}, nil
}

func (s service) SavePlayer(storeScoreRequest *leaderboardpb.StoreScoreRequest) error {
	player := s.createNewPlayer(storeScoreRequest)
	found, e := s.playerRepository.FindByName(player.Name)
	if e != gorm.ErrRecordNotFound {
		if e != nil {
			return e
		}
		if found.Score < player.Score {
			player.Id = found.Id
		} else {
			return nil
		}
	}
	return s.playerRepository.Save(player)
}

func (s service) createNewPlayer(storeScoreRequest *leaderboardpb.StoreScoreRequest) Player {
	id, _ := uuid.NewUUID()
	player := Player{
		Id:    id.String(),
		Name:  storeScoreRequest.Player.GetName(),
		Score: storeScoreRequest.Player.GetScore(),
	}

	return player
}

func (s service) collectPlayersAround(name string) ([]entity.Player, error) {
	player, e := s.playerRepository.FindByName(name)
	if e == nil {
		more, e := s.playerRepository.FindByScoreMore(player.Score, 3)
		if e != nil {
			return nil, e
		}
		less, e := s.playerRepository.FindByScoreLess(player.Score, 3)
		if e != nil {
			return nil, e
		}
		res := append(less, append(more, player)...)
		sort.Slice(res, func(i, j int) bool {
			return res[i].Score > res[j].Score
		})
		return res, nil
	}
	return nil, e
}

func (s service) alreadyFound(players []entity.Player, name string) bool {
	for _, value := range players {
		if value.Name == name {
			return true
		}
	}
	return false
}
