package entity

import leaderboardpb "bidstack.hw/protobuf"

type Player struct {
	Id    string
	Name  string
	Score int64
	Rank  int64
}

func (p Player) MapToProtoPlayer() *leaderboardpb.Player {
	return &leaderboardpb.Player{
		Name:  p.Name,
		Score: p.Score,
		Rank:  p.Rank,
	}
}
