CREATE TABLE IF NOT EXISTS players
(
    id    varchar(50) PRIMARY KEY,
    name  VARCHAR(50) NOT NULL,
    score integer     NOT NULL
);

CREATE INDEX ON players (score);

INSERT into players
values ('id1', 'name1', 1);
INSERT into players
values ('id2', 'name2', 2);
INSERT into players
values ('id3', 'name3', 3);
INSERT into players
values ('id4', 'name4', 4);
INSERT into players
values ('id5', 'name5', 5);
INSERT into players
values ('id6', 'name6', 6);
INSERT into players
values ('id7', 'name7', 7);
INSERT into players
values ('id8', 'name8', 8);
INSERT into players
values ('id9', 'name9', 9);
INSERT into players
values ('id10', 'name10', 10);
INSERT into players
values ('id11', 'name11', 11);
INSERT into players
values ('id12', 'name12', 12);
INSERT into players
values ('id13', 'name13', 13);
INSERT into players
values ('id14', 'name14', 14);
INSERT into players
values ('id15', 'name15', 15);
INSERT into players
values ('id16', 'name16', 16);
INSERT into players
values ('id17', 'name17', 17);
INSERT into players
values ('id18', 'name18', 18);