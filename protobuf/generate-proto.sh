#!/usr/bin/env bash

docker pull namely/protoc-all

docker run -v `pwd`:/defs namely/protoc-all -d ./ -l go -o ./
