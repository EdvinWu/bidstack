package main

import (
	"bidstack.hw/client/conf"
	"bidstack.hw/client/service"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

func main() {

	flag := conf.LoadProfileFlag()
	config := conf.NewConfig(flag)

	lbConnectionService := service.NewLeaderboardClient(config.Backend)

	lbApi := NewLeaderboardApi(lbConnectionService)

	r := mux.NewRouter()
	handleRoutes(r, lbApi)

	log.Printf("App is up and running on port: %v", config.Server.Port)
	if err := http.ListenAndServe(fmt.Sprintf(":%s", config.Server.Port), r); err != nil {
		log.Fatal(err)
	}

}

func handleRoutes(r *mux.Router, leaderboardApi LeaderboardApi) {
	r.HandleFunc(leaderboardApi.StoreScore()).Methods(http.MethodPost)
	r.HandleFunc(leaderboardApi.GetPlayers()).Methods(http.MethodGet)
}
