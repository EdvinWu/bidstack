package models

type Player struct {
	Name  string `json:"name"`
	Score int64  `json:"score"`
	Rank  int64  `json:"rank"`
}

type StoreScoreRequest struct {
	Players []Player
}

type StoreScoreResponse struct {
	Name  string `json:"name"`
	Score int64  `json:"score"`
	Rank  int64  `json:"rank"`
}

type GetPlayersResponse struct {
	Results  []Player `json:"results,omitempty"`
	AroundMe []Player `json:"around_me,omitempty"`
	NextPage int64    `json:"next_page"`
}
