package main

import (
	"bidstack.hw/client/models"
	"bidstack.hw/client/service"
	"encoding/json"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"log"
	"net/http"
)

var getPlayersEndpoint = "/players"

type LeaderboardApi interface {
	StoreScore() (string, func(http.ResponseWriter, *http.Request))
	GetPlayers() (string, func(http.ResponseWriter, *http.Request))
}

type leaderboardApi struct {
	leaderboardClient service.LeaderboardClient
}

func NewLeaderboardApi(leaderboardClient service.LeaderboardClient) LeaderboardApi {
	return leaderboardApi{leaderboardClient: leaderboardClient}
}

func (la leaderboardApi) StoreScore() (string, func(http.ResponseWriter, *http.Request)) {
	return getPlayersEndpoint, func(w http.ResponseWriter, r *http.Request) {
		var ssreq models.StoreScoreRequest
		if err := json.NewDecoder(r.Body).Decode(&ssreq); err != nil {
			log.Println("Can't parse json to model")
		}
		if err := la.leaderboardClient.DoStoreScoreRequest(ssreq, r.Header.Get(service.HeaderApiKey)); err != nil {
			la.checkGrpcErrors(err, w)
		} else {
			la.writeHeaders(w, http.StatusCreated)
		}
	}
}

func (la leaderboardApi) GetPlayers() (string, func(http.ResponseWriter, *http.Request)) {
	return getPlayersEndpoint, func(w http.ResponseWriter, r *http.Request) {
		if gpres, err := la.leaderboardClient.DoGetPlayersRequest(
			r.FormValue("name"),
			r.FormValue("page"),
			r.FormValue("pageSize"),
			r.Header.Get("Api-Key"),
		); err != nil {
			la.checkGrpcErrors(err, w)
		} else {
			if err := json.NewEncoder(w).Encode(gpres); err != nil {
				la.writeHeaders(w, http.StatusInternalServerError)
			}
		}

	}
}

func (la leaderboardApi) checkGrpcErrors(err error, w http.ResponseWriter) {
	if err, found := status.FromError(err); found {
		switch err.Code() {
		case codes.Unauthenticated:
			la.writeHeaders(w, http.StatusUnauthorized)
		case codes.NotFound:
			la.writeHeaders(w, http.StatusNotFound)
		default:
			la.writeHeaders(w, http.StatusInternalServerError)
		}
	}
}

func (la leaderboardApi) writeHeaders(w http.ResponseWriter, header int) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(header)
}
