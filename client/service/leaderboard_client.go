package service

import (
	"bidstack.hw/client/conf"
	"bidstack.hw/client/models"
	leaderboardpb "bidstack.hw/protobuf"
	"context"
	"encoding/base64"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"log"
	"strconv"
)

const HeaderApiKey = "Api-Key"

type LeaderboardClient interface {
	DoStoreScoreRequest(ssreq models.StoreScoreRequest, apiKey string) error
	DoGetPlayersRequest(name, page, pageSize, apiKey string) (models.GetPlayersResponse, error)
}

type leaderboardClient struct {
	settings conf.BackendSettings
}

func NewLeaderboardClient(settings conf.BackendSettings) LeaderboardClient {
	return leaderboardClient{settings: settings}
}

func (lc leaderboardClient) DoStoreScoreRequest(ssreq models.StoreScoreRequest, apiKey string) error {

	conn, connClose := lc.openClientConnection()
	defer connClose()

	client := leaderboardpb.NewLeaderboardServiceClient(conn)
	ctx := lc.createMetaData(apiKey)

	stream, err := client.StorePlayers(ctx)
	if err != nil {
		log.Fatalf("Error while opening stream and calling StoreScore: %v", err)
	}
	for _, player := range ssreq.Players {
		_ = stream.Send(&leaderboardpb.StoreScoreRequest{
			Player: &leaderboardpb.Player{
				Name:  player.Name,
				Score: player.Score,
			},
		})
	}

	_, err = stream.CloseAndRecv()
	if err != nil {
		log.Printf("Error while receiving response: %v", err)
		return err
	} else {
		return err
	}
}

func (lc leaderboardClient) DoGetPlayersRequest(name, page, pageSize, apiKey string) (models.GetPlayersResponse, error) {

	conn, connClose := lc.openClientConnection()
	defer connClose()

	client := leaderboardpb.NewLeaderboardServiceClient(conn)
	ctx := lc.createMetaData(apiKey)

	pageNum, _ := strconv.Atoi(page)
	size, _ := strconv.Atoi(pageSize)

	req := &leaderboardpb.GetPlayerRequest{
		Name:     name,
		Page:     int64(pageNum),
		PageSize: int64(size),
	}
	res, err := client.GetPlayers(ctx, req)
	if err != nil {
		log.Printf("Error while opening stream and calling StoreScore: %v", err)
		return models.GetPlayersResponse{}, err
	} else {
		return models.GetPlayersResponse{
			Results:  lc.mapProtoPlayersToJson(res.PagedPlayers),
			AroundMe: lc.mapProtoPlayersToJson(res.PlayersAround),
			NextPage: res.NextPage,
		}, nil
	}
}

func (lc leaderboardClient) createMetaData(apiKey string) context.Context {
	encodedApiKEy := base64.StdEncoding.EncodeToString([]byte(apiKey))
	return metadata.NewOutgoingContext(context.Background(), metadata.New(map[string]string{HeaderApiKey: encodedApiKEy}))
}

func (lc leaderboardClient) mapProtoPlayersToJson(pp []*leaderboardpb.Player) []models.Player {
	var jp = make([]models.Player, 0)
	for _, p := range pp {
		jp = append(jp, models.Player{Name: p.Name, Score: p.Score, Rank: p.Rank})
	}
	return jp
}

func (lc leaderboardClient) openClientConnection() (*grpc.ClientConn, func() error) {
	serverUrl := fmt.Sprintf("%s:%s",
		lc.settings.Host,
		lc.settings.Port,
	)
	cc, err := grpc.Dial(serverUrl, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not connect: %v", err)
	}
	return cc, cc.Close
}
