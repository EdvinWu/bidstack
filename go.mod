module bidstack.hw

go 1.13

require (
	github.com/golang-migrate/migrate/v4 v4.7.0
	github.com/golang/protobuf v1.3.1
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.1
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.0
	github.com/jinzhu/gorm v1.9.11
	github.com/spf13/viper v1.4.0
	github.com/stretchr/testify v1.3.0
	google.golang.org/grpc v1.21.0
)
